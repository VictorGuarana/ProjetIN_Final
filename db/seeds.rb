# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create(name: 'admin',
    email: 'admin@admin.com',
    password_digest: BCrypt::Password.create('123456'),
    admin: true)

10.times do
    User.create(name: Faker::Simpsons.character,
        email: Faker::Internet.email,
        password_digest: BCrypt::Password.create('123456'),
        admin: false)
    end

10.times do |i|
    Article.create(title: 'teste '+i.to_s,
        content: Faker::Simpsons.quote,
        user_id: rand(2..11))
    end

Tag.create(name: 'Rails')
Tag.create(name: 'Ionic')
Tag.create(name: 'Banco de dados')

10.times do |i|
    Question.create(title: 'Questão '+i.to_s,
        content: Faker::StarWars.quote,
        user_id: rand(2..11))
end

15.times do
    TagQuestion.create(question_id: rand(1..11),
    tag_id: rand(1..3))
end
    
5.times do |i|
    Cell.create(name: 'Celula '+i.to_s)
end

10.times do |i|
    DirectionUser.create(user_id: i,
        direction_id: rand(1..6),
        active: true)
end

Direction.create(name: 'Presidencia')

5.times do |i|
    Direction.create(name: 'Diretoria '+i.to_s)
end

10.times do |i|
    DirectionUser.create(user_id: i,
        direction_id: rand(1..6),
        active: true)
end