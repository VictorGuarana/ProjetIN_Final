class CreateCellUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :cell_users do |t|
      t.references :cell, foreign_key: true
      t.references :user, foreign_key: true
      t.boolean :manager
      t.boolean :active

      t.timestamps
    end
  end
end
