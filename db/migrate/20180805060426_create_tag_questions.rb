class CreateTagQuestions < ActiveRecord::Migration[5.2]
  def change
    create_table :tag_questions do |t|
      t.references :question, foreign_key: true
      t.references :tag, foreign_key: true

      t.timestamps
    end
  end
end
