class CreateDirectionUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :direction_users do |t|
      t.references :user, foreign_key: true
      t.references :direction, foreign_key: true
      t.boolean :director
      t.boolean :active
      
      t.timestamps
    end
  end
end
