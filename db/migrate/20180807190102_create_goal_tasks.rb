class CreateGoalTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :goal_tasks do |t|
      t.string :name
      t.references :goal, foreign_key: true
      t.boolean :check

      t.timestamps
    end
  end
end
