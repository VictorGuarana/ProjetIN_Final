class CreateProjectTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :project_tasks do |t|
      t.string :content
      t.boolean :status
      t.references :project, foreign_key: true
      
      t.timestamps
    end
  end
end
