Rails.application.routes.draw do
  



  get 'cells/index'
  get 'cells/edit'
  resources :goals
  resources :projects
  resources :questions
  resources :articles
  resources :users



  #Sessions routes  
  get 'sessions/new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  root 'sessions#home'

  
  #Questions routes
  post '/addquestiontag', to: 'questions#addtag', as: :addquestiontag
  post '/addanswer', to: 'questions#add_answer', as: :addanswer
  delete '/tagquestiondestroy', to: 'questions#destroy_tag', as: :destroyquestiontag
  delete '/destroyanswer/:id', to: 'questions#destroy_answer', as: :destroyanswer
  post '/likequestion/:id', to: 'questions#like', as: :likequestion
  post '/bestanswer', to: 'questions#best_answer', as: :bestanswer

  #Articles routes
  post '/addarticletag', to: 'articles#addtag', as: :addarticletag
  post '/addcomment', to: 'articles#add_comment', as: :addcomment
  delete '/destroycomment', to: 'articles#destroy_comment', as: :destroycomment
  delete '/tagarticledestroy', to: 'articles#destroy_tag', as: :destroyarticletag
  post '/likearticle/:id', to: 'articles#like', as: :likearticle

  #Project routes
  post '/addprojectmember', to: 'projects#add_member', as: :addprojectmember
  delete '/removemember', to: 'projects#remove_member', as: :removemember
  post '/addprojecttask', to: 'projects#add_task', as: :addprojecttask
  delete '/removetask', to: 'projects#remove_task', as: :removetask
  post '/projectdone', to: 'projects#done', as: :projectdone

  #Goals routes
  post '/addgoaltask', to: 'goals#add_task', as: :addgoaltask
  delete '/removetask/:id', to: 'goals#remove_task', as: :remove_task
  post '/checktask/:id', to: 'goals#check_task', as: :check_task

  #admin routes
  get 'admin/page', as: :admin
  post '/criadiretoria', to: 'admin#cria_diretoria', as: :criadiretoria
  post '/addmembrodiretoria', to: 'admin#add_membro_diretoria', as: :addmembrodiretoria
  post '/criacelula', to: 'admin#cria_celula', as: :criacelula
  post '/addmembrocelula', to:'admin#add_membro_celula', as: :addmembrocelula
  
  #Directions routes
  get 'directions/index', as: :directions
  get 'directions/edit/:id',to: 'directions#edit', as: :edit_directions
  post '/editadiretoria', to: 'directions#edita_diretoria', as: :editadiretoria
  post '/add_membro_diretoria', to: 'directions#add_membro_diretoria', as: :add_membro_diretoria

  #Cells routes 
  get 'cells/index', as: :cells
  get 'cells/edit/:id',to: 'cells#edit', as: :edit_cells
  post '/editacelula', to: 'cells#edita_celula', as: :editacelula
  post '/add_membro_celula', to: 'cells#add_membro_celula', as: :add_membro_celula
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
