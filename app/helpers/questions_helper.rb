module QuestionsHelper

    def answer_user(user)
        @answer_user = User.find(user)
    end

    def question_likes(question)
        @question_likes = LikeQuestion.where(question_id: question.id)
    end

    def user_already_like(user_id, question_id)
        @alreadylike = LikeQuestion.where(user_id: user_id)
        @alreadylike = @alreadylike.find_by_question_id(question_id)
    end

    def has_best(question_id)
        @answers = Answer.where(question_id: question_id)
        @answers = @answers.find_by_best(true)
    end
end
