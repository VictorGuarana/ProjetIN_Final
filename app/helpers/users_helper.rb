module UsersHelper
    def article_likes(article)
        @article_likes = LikeArticle.where(article_id: article.id)
    end
    
    def article_likes_total(user)
        @articles = Article.where(user_id: user.id) #procura artigos do user
        @article_likes = 0
        @articles.each do |article| #conta likes dos artigos
            @article_likes += LikeArticle.where(article_id: article.id).size 
        end
        @article_likes #retorna isso
    end

    def user_projects_done(user)
        @projects = ProjectUser.where(user_id: user.id)
        @userprojectsdone ||= []
        @projects.each do |project|
            @project = Project.find(project.project_id)
            if @project.done
            @userprojectsdone.push(@project)
            end
        end
        @userprojectsdone
    end

    def user_projects_undone(user)
        @projects = ProjectUser.where(user_id: user.id)
        @userprojects ||= []
        @projects.each do |project|
            @project = Project.find(project.project_id)
            if !@project.done
            @userprojects.push(@project)
            end
        end
        @userprojects
    end

    def user_direction(user_id)
        @user_directions = DirectionUser.where(user_id: user_id, active: true)
        @user_direction = @user_directions.first
        if @user_direction != nil
            @direction = Direction.find(@user_direction.direction_id)
        end
    end




end
