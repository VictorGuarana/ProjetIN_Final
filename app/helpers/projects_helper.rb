module ProjectsHelper

    def projectmembers
        @project_id = params[:id]
        @projectusers = ProjectUser.where(project_id: @project_id)
        @members ||= []
        @projectusers.each do |user|
            @members.push(User.find(user.user_id))
        end
        @members #retornando array com users
    end

    def projecttasks
        @project_id = params[:id]
        @tasks = ProjectTask.where(project_id: @project_id) 
    end

    def member_cell(member_id)
        @user_cells = CellUser.where(user_id: member_id)
        if @user_cells != nil
            @cell = @user_cells.find_by_active(true)
            @cell = Cell.find(@cell.cell_id)
        end
    end

    def manager_id()
    end
    
end
