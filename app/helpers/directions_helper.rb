module DirectionsHelper
    def diretor(direction_id)
        @direction_lines = DirectionUser.where(direction_id: direction_id)
        @director_line = @direction_lines.find_by_director(true)
        if @director_line != nil
            @director = User.find(@director_line.user_id)
        end
    end

    def presidente()
        @direction_line = Direction.where(name: 'presidencia')
        if @direction_line.first != nil
            @direction_id = @direction_line.first.id    
            @direction_lines = DirectionUser.where(direction_id: @direction_id)
            @director_line = @direction_lines.find_by_director(true)
            if @director_line != nil
                @director = User.find(@director_line.user_id)
            end
        end
    end

    def membros(direction_id)
        @direction_lines = DirectionUser.where(direction_id: direction_id)
        @member = []
        @direction_lines.each do |line|
            @member.push(User.find(line.user_id))
        end
        @member
    end
    
end
