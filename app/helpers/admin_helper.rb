module AdminHelper

    def membros_diretoria(direction_id)
        @membros_linhas = DirectionUser.where(direction_id: direction_id)
        @membros = []
        @membros_linhas.each do |linha|
            @membros.push(User.find(linha.user_id))
        end
        @membros
    end

    def director?(direction_id, user_id)
        @membros_diretoria = DirectionUser.where(direction_id: direction_id)
        @director = @membros_diretoria.find_by_director(true)
        if @director != nil
            @director.user_id == user_id
        end
    end

    def membros_celula(cell_id)
        @membros_linhas = CellUser.where(cell_id: cell_id)
        @membros = []
        @membros_linhas.each do |linha|
            @membros.push(User.find(linha.user_id))
        end
        @membros
    end

    def manager?(cell_id, user_id)
        @membros_celula = CellUser.where(cell_id: cell_id)
        @manager = @membros_celula.find_by_manager(true)
        if @manager != nil
            @manager.user_id == user_id
        end
    end

end
