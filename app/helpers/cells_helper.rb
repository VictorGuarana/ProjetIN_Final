module CellsHelper
    
    def gerente(cell_id)
        @cell_lines = CellUser.where(cell_id: cell_id)
        @manager_line = @cell_lines.find_by_manager(true)
        if @manager_line != nil
            @manager = User.find(@manager_line.user_id)
        end
    end

    def membros(cell_id)
        @cell_lines = CellUser.where(cell_id: cell_id)
        @member = []
        @cell_lines.each do |line|
            @member.push(User.find(line.user_id))
        end
        @member
    end
    
end
