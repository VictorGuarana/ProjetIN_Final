module ArticlesHelper
    def comment_user(user) #pega usuario dono do comment
        @comment_user = User.find(user)
    end

    def article_likes(article) #retorna aray de likes do artigo
        @article_likes = LikeArticle.where(article_id: article.id)
    end

    def user_already_like(user_id, article_id) #confere se user ja curtiu
        @alreadylike = LikeArticle.where(user_id: user_id)
        @alreadylike = @alreadylike.find_by_article_id(article_id)
    end
end
