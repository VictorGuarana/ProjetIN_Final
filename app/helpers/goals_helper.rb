module GoalsHelper
    
    def tasks(goal_id)
        @tasks = GoalTask.where(goal_id: goal_id)
    end

end
