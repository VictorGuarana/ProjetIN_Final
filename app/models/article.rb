class Article < ApplicationRecord
  belongs_to :user
  has_many :like_article, dependent: :destroy
  has_many :comment, dependent: :destroy
  has_many :tag_article, dependent: :destroy
end
