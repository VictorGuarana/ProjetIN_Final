class Goal < ApplicationRecord
    has_many :goal_task, dependent: :destroy
end
