class Question < ApplicationRecord
  belongs_to :user
  has_many :like_question, dependent: :destroy
  has_many :answer, dependent: :destroy
  has_many :tag_question, dependent: :destroy
end
