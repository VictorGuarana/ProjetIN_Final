class Project < ApplicationRecord
    has_many :project_task, dependent: :destroy
    has_many :project_user, dependent: :destroy
end
