class ArticlesController < ApplicationController
  include ArticlesHelper
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  before_action :expulsa_nao_logados

  # GET /articles
  # GET /articles.json
  def index
    @articles = Article.all
  end

  # GET /articles/1
  # GET /articles/1.json
  def show
    #mostrar tags do artigo
    @tags ||= []
    TagArticle.all.each do |tag_article|
      @tag = Tag.all.find(tag_article.tag_id)
      if tag_article.article_id == @article.id
        @tags.push(@tag)
      end
    end

    #add tags
    @tag_article = TagArticle.new
    @tag_all = Tag.all

    #adicionar comentarios
    @comment = Comment.new

    #ver comentarios
    @comments ||= []
    Comment.all.each do |comment|
      if comment.article_id == @article.id
        @comments.push(comment)
      end
    end
      
  end

  # GET /articles/new
  def new
    @article = Article.new
  end

  # GET /articles/1/edit
  def edit
  end

  # POST /articles
  # POST /articles.json
  def create
    @article = Article.new(article_params)
    @article.user_id = current_user.id

    respond_to do |format|
      if @article.save
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /articles/1
  # PATCH/PUT /articles/1.json
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  def destroy
    @article.destroy
    respond_to do |format|
      format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

    
  def addtag
    @article_id = params[:tag_article][:article_id]
    @tag_id = params[:tag_article][:tag_id]
    @tagarticles = TagArticle.where(article: @article_id)
    @tagarticles = @tagarticles.find_by_tag_id(@tag_id)
    if @tagarticles == nil
      TagArticle.create({article_id: @article_id, tag_id: @tag_id})
    end
    redirect_to article_path(params[:tag_article][:article_id])
  end

  def add_comment
    Comment.create({content: params[:comment][:content], article_id: params[:comment][:article_id], user_id: current_user.id })
    redirect_to article_path(params[:comment][:article_id])
  end

  def destroy_tag
    @article_id = params[:article]
    @tag_id = params[:tag]
    @tagarticles = TagArticle.where(article: @article_id)
    @tagarticles = @tagarticles.find_by_tag_id(@tag_id)
    @tagarticles.destroy
    redirect_to article_path(@article_id)
  end

  def destroy_comment
    @comment = Comment.find(params[:comment])
    @comment.destroy
    @article_id = params[:article]
    redirect_to article_path(@article_id)
  end

  def like
    @article_id = params[:id]
    @likearticle = user_already_like(current_user.id, @article_id)
    if @likearticle.nil?
      LikeArticle.create(user_id: current_user.id, article_id: @article_id)
    else
      @likearticle.destroy
    end
    redirect_to article_path(@article_id)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_article
      @article = Article.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def article_params
      params.require(:article).permit(:title, :content)
    end
end
