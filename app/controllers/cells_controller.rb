class CellsController < ApplicationController

  before_action :expulsa_nao_logados
  
  def index
  end

  def edit

    @current_cell = Cell.find(params[:id])

    #editar celula
    @cell = Cell.new

    #adiciona membros
    @cell_user = CellUser.new
  end

  def edita_celula
    @cell = Cell.find(params[:cell][:cell_id])
    @cell.name = params[:cell][:name]
    @cell.save
    redirect_to edit_cells_path(params[:cell][:cell_id])
  end

  def add_membro_celula
    @user_id = params[:cell_user][:user_id]
    @cell_id = params[:cell_user][:cell_id]
    @manager = params[:cell_user][:manager]
    CellUser.create(user_id: @user_id, cell_id: @cell_id, manager: @manager, active: true)
    redirect_to edit_cells_path(@cell_id)
  end

end
