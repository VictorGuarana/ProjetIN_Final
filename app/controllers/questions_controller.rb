class QuestionsController < ApplicationController
  include QuestionsHelper
  
  before_action :set_question, only: [:show, :edit, :update, :destroy]
  before_action :expulsa_nao_logados

  # GET /questions
  # GET /questions.json
  def index
    @questions = Question.all
  end

  # GET /questions/1
  # GET /questions/1.json
  def show
    #tags de cada questão
    @tags ||= []
    TagQuestion.all.each do |tag_question|
      @tag = Tag.all.find(tag_question.tag_id)
      if tag_question.question_id == @question.id
        @tags.push(@tag)
      end
    end

      #selecionar tags
    @tag_question = TagQuestion.new
    @tag_all = Tag.all

    #visualizar comentarios
    @answers = []
    Answer.all.each do |answer|
      if answer.question_id == @question.id
        @answers.push(answer)
      end
    end

    #criar comentarios/respostas
    @answer = Answer.new
    
  end

  # GET /questions/new
  def new
    @question = Question.new
  end

  # GET /questions/1/edit
  def edit
  end

  # POST /questions
  # POST /questions.json
  def create
    @question = Question.new(question_params)
    @question.user_id = current_user.id

    respond_to do |format|
      if @question.save
        format.html { redirect_to @question, notice: 'Question was successfully created.' }
        format.json { render :show, status: :created, location: @question }
      else
        format.html { render :new }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /questions/1
  # PATCH/PUT /questions/1.json
  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /questions/1
  # DELETE /questions/1.json
  def destroy
    @question.destroy
    respond_to do |format|
      format.html { redirect_to questions_url, notice: 'Question was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  
  def like
    @question_id = params[:id]
    @likequestion = user_already_like(current_user.id, @question_id)
    if @likequestion.nil?
      LikeQuestion.create(user_id: current_user.id, question_id: @question_id)
    else
      @likequestion.destroy
    end
    redirect_to question_path(@question_id)
  end

  def addtag
    @question_id = params[:tag_question][:question_id]
    @tag_id = params[:tag_question][:tag_id]
    @tagquestions = TagQuestion.where(question: @question_id)
    @tagquestions = @tagquestions.find_by_tag_id(@tag_id)
    if @tagquestions == nil
      TagQuestion.create({question_id:  @question_id, tag_id: @tag_id})
    end
    redirect_to question_path(@question_id)
  end

  def add_answer
    @question_id = params[:answer][:question_id]
    @content = params[:answer][:content]
    Answer.create({content: @content, user_id: current_user.id, question_id: @question_id, best: false})
    redirect_to question_path(@question_id)
  end

  def destroy_tag
    @question_id = params[:question]
    @tag_id = params[:tag]
    @tagquestions = TagQuestion.where(question: @question_id)
    @tagquestions = @tagquestions.find_by_tag_id(@tag_id)
    @tagquestions.destroy
  end

  def destroy_answer
    @answer = Answer.find(params[:id])
    @answer.destroy
    #adiocionar caminho
  end

  def best_answer
    @answer = Answer.find(params[:answer])
    @question_id = params[:question]
    @best_answer = has_best(@question_id)
    if @best_answer.nil?
      @answer.best = true
    else 
      if !@answer.best
        @answer.best = true
        @best_answer.best = false
        @best_answer.save
      else
        @answer.best = false
      end
    end
    @answer.save
    redirect_to question_path(@question_id)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_question
      @question = Question.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def question_params
      params.require(:question).permit(:title, :content)
    end

    def tags_per_question
      @tags = []
      TagQuestion.all.each do |tag_question|
        if tag_question.question_id == @question.id
          @tags << Tag.all.find(tag_question.tag_id)
        end
      end
    end
end
