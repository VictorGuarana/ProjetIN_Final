class AdminController < ApplicationController
  include AdminHelper

  before_action :expulsa_nao_logados

  def page
    #cria diretoria 
    @direction = Direction.new

    #adciona membro a diretoria
    @direction_user = DirectionUser.new

    #cria celula
    @cell = Cell.new

    #adiciona membro celula
    @cell_user = CellUser.new

  end

  def cria_diretoria
    Direction.create(name: params[:direction][:name])
    redirect_to admin_path
  end
  
  def add_membro_diretoria
    @user_id = params[:direction_user][:user_id]
    @direction_id = params[:direction_user][:direction_id]
    @director = params[:direction_user][:director]
    DirectionUser.create(user_id: @user_id, direction_id: @direction_id, director: @director, active: true)
    redirect_to admin_path
  end

  def cria_celula
    Cell.create(name: params[:cell][:name])
    redirect_to admin_path
  end

  def add_membro_celula
    @user_id = params[:cell_user][:user_id]
    @cell_id = params[:cell_user][:cell_id]
    @manager = params[:cell_user][:manager]
    CellUser.create(user_id: @user_id, cell_id: @cell_id, manager: @manager, active: true)
    redirect_to admin_path
  end

end
