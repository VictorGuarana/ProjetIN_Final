class DirectionsController < ApplicationController

  before_action :expulsa_nao_logados
  
  def index
  end

  def edit

    @current_direction = Direction.find(params[:id])

    #editar diretoria
    @direction = Direction.new

    #adiciona membros
    @direction_user = DirectionUser.new
  end

  def edita_diretoria
    @direction = Direction.find(params[:direction][:direction_id])
    @direction.name = params[:direction][:name]
    @direction.save
    redirect_to edit_directions_path(params[:direction][:direction_id])
  end

  def add_membro_diretoria
    @user_id = params[:direction_user][:user_id]
    @direction_id = params[:direction_user][:direction_id]
    @director = params[:direction_user][:director]
    DirectionUser.create(user_id: @user_id, direction_id: @direction_id, director: @director, active: true)
    redirect_to edit_directions_path(@direction_id)
  end

end
