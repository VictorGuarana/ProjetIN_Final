class ProjectsController < ApplicationController
  include ProjectsHelper
  before_action :set_project, only: [:show, :edit, :update, :destroy]
  before_action :expulsa_nao_logados

  # GET /projects
  # GET /projects.json
  def index
    @projects = Project.all
  end

  # GET /projects/1
  # GET /projects/1.json
  def show
  end

  # GET /projects/new
  def new
    @project = Project.new
  end

  # GET /projects/1/edit
  def edit
    #adicionar membros
    @project_user = ProjectUser.new

    #adicionar terefa
    @project_task = ProjectTask.new
  end

  # POST /projects
  # POST /projects.json
  def create
    @project = Project.new(project_params)

    respond_to do |format|
      if @project.save
        format.html { redirect_to @project, notice: 'Project was successfully created.' }
        format.json { render :show, status: :created, location: @project }
      else
        format.html { render :new }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /projects/1
  # PATCH/PUT /projects/1.json
  def update
    respond_to do |format|
      if @project.update(project_params)
        format.html { redirect_to @project, notice: 'Project was successfully updated.' }
        format.json { render :show, status: :ok, location: @project }
      else
        format.html { render :edit }
        format.json { render json: @project.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /projects/1
  # DELETE /projects/1.json
  def destroy
    @project.destroy
    respond_to do |format|
      format.html { redirect_to projects_url, notice: 'Project was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_member
    @user_id = params[:project_user][:user_id]
    @project_id = params[:project_user][:project_id]
    ProjectUser.create(project_id: @project_id, user_id: @user_id)
    redirect_to edit_project_path(@project_id)
  end

  def remove_member
    @user_id = params[:member]
    @project_id = params[:project]
    @projectmember = ProjectUser.where(project_id: @project_id)
    @projectmember = @projectmember.find_by_user_id(@user_id)
    @projectmember.destroy
    redirect_to edit_project_path(@project_id)
  end

  def add_task
    @task_content = params[:project_task][:content]
    @project_id = params[:project_task][:project_id]
    ProjectTask.create(project_id: @project_id, content: @task_content)
    redirect_to edit_project_path(@project_id)
  end

  def remove_task
    @task_id = params[:task]
    @project_id = params[:project]
    @task = ProjectTask.find(@task_id)
    @task.destroy
    redirect_to edit_project_path(@project_id)
  end

  def done
    @project_id = params[:project]
    @project = Project.find(@project_id)
    if !@project.done
      @project.done = true
    else
      @project.done = false
    end
    @project.save
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_project
      @project = Project.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def project_params
      params.require(:project).permit(:name, :description, :done)
    end
end
